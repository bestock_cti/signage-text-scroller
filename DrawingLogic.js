

window.ScrollerCanvas = function (opt) {
    //  Force 'new window.ScrollerCanvas()' vs 'window.ScrollerCanvas()'
    if ( (this instanceof window.ScrollerCanvas) == false ) {
        return new window.ScrollerCanvas(opt);
    }

    var elDic = {},
        canvasEl = document.getElementById(opt.id);

    return {
        draw: function ( lines ) {
            for(var i = 0; i < lines.length; i++) {
                var line = lines[i];
                var right = line.left - line.width;
                //console.log("Right", right);

                if ( right > (opt.pos.right + 10) ) {
                    //  REMOVE IT FROM OUR CANVAS AND/OR SKIP DRAWING IT
                    if ( elDic.hasOwnProperty(line.value) && elDic[line.value] != null ) {                        
                        //  REMOVE THE ELEMENT
                        canvasEl.removeChild(elDic[line.value]);
                        elDic[line.value] = null;
                    }
                    //  SKIP DRAWING IT
                    continue;
                }


                //  This adds the padding BACK from p0
                if ( (line.left) < opt.pos.left ) {
                    //  SKIP DRAWING IT
                    continue;
                }

                //  DRAW THE ELEMENT
                if ( elDic.hasOwnProperty(line.value) == false || elDic[line.value] == null ) {
                    var _ref = elDic[line.value] = document.createElement("div");

                    _ref.style.position = "absolute"
                    _ref.style.height = line.height + "px";
                    _ref.style.width = (line.width + 10) + "px";
                    _ref.innerHTML = line.value;
                    canvasEl.appendChild(_ref);
                }

                var lineEl = elDic[line.value];
                var offsetLeft = (opt.pos.right - line.left) + 15;
               
                lineEl.style.left = offsetLeft + "px";
            }
        }
    }
};


window.Scroller = (function (data) {
    var intervalToken = null,
        delay = window.SCROLLER_DELAY_IN_MS,
        padding = window.SCROLLER_PADDING_IN_PX,
        step = window.SCROLLER_STEP_IN_PX,
        lines = [], //  lines currently being rendered
        canvasList = [],
        head = 0,   //  furthest LEFT point  (away from 0)
        tail = 0,   //  furthest RIGHT point (towards 0)
        fullWidth = 0;
    
    function _draw_loop_() {

        //  ====== ADD AND REMOVE LINES =======
        if ( lines.length == 0 || tail > padding ) {
            //  The right corner of text has gone beyond the spacing distance; add a new line
            lines.push(data.getNext());
        }

        lines = lines.filter(function (line) {
            if ( (line.left - line.width) > (fullWidth + 25) ) {
                return false;
            }
            return true;
        })

        //  ====== UPDATE HEAD AND TAIL ========
        var nHead = head,
            nTail = null;

        for(var i = 0; i < lines.length; i++) {
            var line = lines[i],
                right = line.left - line.width;

            line.left = line.left + step;

            if ( nTail == null || (right) < nTail ) {   //  IF line.right < nTail; THEN nTail = line.right
                nTail = right;
            }

            if ( line.left > nHead) {
                nHead = line.left;
            }
        }

        tail = nTail;
        head = nHead;

        //  ====== TRIGGER REDRAW ========

        for(var i = 0; i < canvasList.length; i++) {
            canvasList[i].draw(lines);
        }
    }


    return {
        setDelay: function ( newDelay ) {
            if ( intervalToken != null ) {
                clearInterval(intervalToken);
            }
            delay = newDelay;
            intervalToken = setInterval(_draw_loop_, delay);
        },
        setPadding: function ( newPadding ) {
            padding = newPadding;
        },
        start: function () {
            intervalToken = setInterval(_draw_loop_, delay)
        },
        stop: function () {
            if ( intervalToken != null ) {
                clearInterval(intervalToken);
            }
        },
        addCanvas: function (opt) {
            var el = document.getElementById(opt.id);
            opt.width = el.clientWidth;
            opt.pos = {
                left: fullWidth,
                right: fullWidth + opt.width
            };
            fullWidth = opt.pos.right + 1;
            canvasList.push(new window.ScrollerCanvas(opt));
        }
    };
})(window.DataFeed);



window.SCROLLER_CANVAS_LIST.reverse().map(c => {
    console.log("Adding canvas ", c);
    window.Scroller.addCanvas({
        id: c
    });
});

//  ---- Start the scroller -----
window.DataFeed.load(function () {
    window.Scroller.start();
});