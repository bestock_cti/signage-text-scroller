
function getTextSize(text) {
    var rulerEl = document.getElementById("ruler");

    rulerEl.innerHTML = text;

    return {
        width: rulerEl.offsetWidth,
        height: rulerEl.offsetHeight
    };
}


window.LineOfText = function ( opt ) {

    //  Force the usage of "new window.LineOfText()" vs "window.LineOfText()";
    if ( (this instanceof window.LineOfText) == false ) {
        return new window.LineOfText(opt);
    }

    var size = getTextSize(opt.value);
    this.width = size.width;
    this.height = size.height;
    this.left = 0;
    this.value = opt.value;
};


window.DataFeed = (function() {

    var data = [
        "Sentance number one.",
        "Sentance number two.",
        "SwItCh uP!",
        "Hello World",
        "Sentance number five.",
       "asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf",
        "Loreasldkfjas;dfkjas;ldfkjas;ldfkja;sldkfja;sfjka;lsdfjka;sfjkas;dfkjas;ldfjk"
    ];

    var pointer = 0;

    return {
        load: function (cb) { 
            console.log("Fake data loaded..."); //  REPLACE THIS SECTION with the real data source
            cb();
        },
        getNext: function () { 
            var val = data[pointer++];

            pointer = pointer % data.length;

            return new window.LineOfText({
                value: val
            });
        },
        addLine: function (value) {
            data.push(value);
        }
    };
})();

console.log("Window.Datafeed", window.DataFeed);